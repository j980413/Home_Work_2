package day02;
/**
 * 输入一个IP地址，然后将4段数字分别输出
 * @author Bonnie
 *
 */
public class Test03 {
	public static void main(String[] args) {
		String ip = "192.168.166.166";
		String regex = "\\.";
		String []ss = ip.split(regex);
		for(String s:ss) {
			System.out.println(s);
		}
	}
}
