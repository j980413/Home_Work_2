package day03;

import java.util.Date;

/**
 * 使用Date输出当前系统时间，以及3天后这一刻的时间
 * @author Bonnie
 *
 */
public class Test01 {
	public static void main(String[] args) {
		Date date = new Date();
		System.out.println(date);//输出当前时间
		date.setTime(date.getTime()+1000*60*60*24*3);//输出三天后的这一刻的时间
		System.out.println(date);
	}
}
